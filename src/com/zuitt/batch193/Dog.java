package com.zuitt.batch193;

public class Dog extends Animal{
    //child class of Animal
    //child class that inherits Animal class
    //"extends" keyword used to inherits variables in the Animal class

    //properties
    private String breed;

    //constructor
    public Dog(){
        //"super" keyword will reference the variable from the Animal class
        super(); //Animal() constructor by using this, we can use the instance variable, or we can invoke immediate parent class constructor and class method
        this.breed = "Chihahua";
    }
    //with parameterized constructor
    public Dog(String name, String color, String breed){
        super(name, color); //Animal(String name, String color) constructor
        this.breed = breed;
    }

    //getter
    public String getBreed() {
        return this.breed;
    }

    //method
    public void speak(){
        super.call(); //The call() method from Animal class will be inherited
        System.out.println("Bark");

    }


}
