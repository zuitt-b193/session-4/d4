package com.zuitt.batch193;

public class Animal {
    //Parent class for inheritance
    //blueprint

    //properties
    private String name;
    private String color;

    //empty constructor
    public Animal (){};
    //parameterized constructor
    public Animal(String name, String color){
        this.name = name;
        this.color = color;
    }

    //getter
    public String getName() {
        return this.name;
    }

    public String getColor() {
        return this.color;
    }

    //setter

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    //method
    public void call(){
        System.out.println("Hi my name is " + getName());
    }

}
