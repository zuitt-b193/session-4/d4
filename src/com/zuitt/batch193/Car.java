package com.zuitt.batch193;

public class Car {

    //blueprint of Car object
    //Class creation - is composed of four parts:
    //1. Properties - characteristic of the object
    //2. Constructors - Used to create an object
    //3. Getters/Setter - to get/retrieve and set/change the values of each property of the object
    //4. Methods - functions of an object where it can perform a task

    //Access modifiers
    //1.Default - no keyword required. Only those classes that are in the same package can access this class. No other class outside the package can access the class
    //2. Private- only accessible within the same file
    //3. Public - the members, methods and classes that are declared public can be accessed from anywhere
    //4. Protected

    //Properties = qualities or characters of real world objects()
    public String name;
    private String brand;
    private int manufactureDate;
    private String owner;
    //make Driver component
    private Driver d;
    //make Passenger component
    private Passenger p;

    //Constructors
    //empty constructor - it is a common practice to create an empty and parameterized constructor for creating new instances
    public Car(){
        //add a driver so whenever a new Car is created, it will always have a driver
        this.d = new Driver("Alejandro");
        //add a passenger as well whenever a new Car is created
        this.p = new Passenger("Lady Gaga");
    };

    //parameterized constructor = to accept and to add properties/parameters
    public Car(String name, String brand, int manufactureDate, String owner) {
        this.name = name;
        this.brand = brand;
        this.manufactureDate = manufactureDate;
        this.owner = owner;
    }

    //add a getter for Driver
    public String getDriverName(){
        return this.d.getName();
    }
    //add a getter for Passenger
    public String getPassengerName(){
        return this.p.getPassengerName();
    }

    //getters - read only
    public String getName(){
        return this.name;
    }
    public String getBrand(){
        return this.brand;
    }
    public int getManufactureDate(){
        return this.manufactureDate;
    }
    public String getOwner(){
        return this.owner;
    }

    //setters - write only or for setting up the properties
    //void - will not return anything
    public void setName(String name){
        this.name = name;
    }
    public void setBrand(String brand){
        this.brand = brand;
    }
    public void setManufactureDate(int manufactureDate){
        this.manufactureDate = manufactureDate;
    }
    public void setOwner(String owner){
        this.owner = owner;
    }

    //Methods
    public void drive(){
        System.out.println(getOwner() + " drives the " + getBrand());
    }
    public void printDetails(){
        System.out.println("This " + getManufactureDate() + " " + getBrand() + " " + getName() + " is owned by " + getOwner());
    }



    //to string concat (right click generate)
    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", manufactureDate=" + manufactureDate +
                ", owner='" + owner + '\'' +
                '}';
    }





}
