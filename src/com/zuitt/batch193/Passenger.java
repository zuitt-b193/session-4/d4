package com.zuitt.batch193;

public class Passenger {

    //blueprint


    //properties
    private String passengerName;

    //empty constructor
    public Passenger(){};

    //parameterized constructor
    public Passenger(String passengerName){
        this.passengerName = passengerName;
    }

    //getter
    public String getPassengerName(){
        return this.passengerName = passengerName;
    }

    //setter
    public String setPassengerName(){
        return this.passengerName = passengerName;
    }
}
