package com.zuitt.batch193;

public class Driver {

    //blueprint

    //properties
    private String name;

    //empty constructor
    public Driver(){};

    //parameterized constructor
    public Driver(String name){
        this.name = name;
    }

    //getter
    public String getName(){
        return this.name;
    }

    //setter
    // not "void" since every time we set a new Car, there is a Driver that expects String
    public String setName(){
        return this.name = name;
    }


}
