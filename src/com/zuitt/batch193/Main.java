package com.zuitt.batch193;

import java.util.Scanner;

public class Main {

    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        //instantiation of object
        //new instance with an empty constructor
        Car myFirstCar = new Car(){};
    //    System.out.println(myFirstCar.getName());
        //Setters
    //    System.out.println("Input myFirstCar name");
    //    myFirstCar.setName(input.nextLine());
    //    System.out.println("myFirstCar name is " + myFirstCar.getName());
        myFirstCar.setName("Civic");
        myFirstCar.setBrand("Honda");
        myFirstCar.setManufactureDate(1998);
        myFirstCar.setOwner("John");


        //new instance of Car with parameterized parameter
        //added the values of the new Car
        Car mySecondCar = new Car("Charger", "Dodge", 1978, "Vin Diesel");
        System.out.println(mySecondCar.toString());

        //we used the getters from the Car class to retrieve the property of the object
        System.out.println(mySecondCar.getName());
        System.out.println(mySecondCar.getBrand());
        System.out.println(mySecondCar.getManufactureDate());
        System.out.println(mySecondCar.getOwner());


        //methods
        myFirstCar.drive();
        mySecondCar.drive();

        myFirstCar.printDetails();
        mySecondCar.printDetails();


        //Encapsulation
        //we bundled each fields and methods inside a single class and we used access modifiers like public to allow the access from another class
        //why we use encapsulation:
        //The fields of a class can be made read only and write only
        //a class can have a total control over what is stored in its field
        //in java, encapsulation helps us to keep related fields and methods together which make our code cleaner and easy to read
        //we can achieve data hiding using the access modifier private

        Car anotherCar = new Car();
        anotherCar.setName("Ferrari");
        System.out.println(anotherCar.getName());


        anotherCar.name = "Honda";
        System.out.println(anotherCar.getName());


        //Composition = Ex: A car has a driver
        //allows modelling objects that are made up of other objects. It defines "HAS a relationship"


        Car newCar = new Car();
        System.out.println("This car is driven by " + newCar.getDriverName());
        System.out.println("A car has a driver " + anotherCar.getDriverName());

        System.out.println("This car HAS a passenger named " + newCar.getPassengerName());
        //every time a new Car() is created, there is a Driver and Passenger


        //Inheritance = Ex: A car is a vehicle
        //allows modelling objects inherit as a subset of another object. It defines "IS A" relationship
        //Inheritance can be defined as the process where one class acquires the properties (methods and fields) of another class
        //"extends" is a keyword used to inherit the properties of a class
        //"super" keyword used for referencing the variable, properties or methods, which can be used to another class

        Dog dog1 = new Dog();
        System.out.println(dog1.getBreed()); //composition

        //No constructor in Dog.java, but it is inherited in Animal.java
        dog1.setName("Brownie");
        dog1.setColor("Brown");
        System.out.println(dog1.getName());
        System.out.println(dog1.getColor());

        Dog doge = new Dog("Blackie", "Black", "Aspin");
        System.out.println(doge.getBreed());

        //method
        doge.call();
        doge.speak(); //both method from Animal and Dog will be present


    }


}
